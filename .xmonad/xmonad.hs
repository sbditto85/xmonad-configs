import XMonad
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.ManageDocks
import XMonad.Util.Run(spawnPipe)
import XMonad.Util.EZConfig(additionalKeys)
import System.IO

-- make sure to edit paths to xmobar and .xmobarrc to match your system.
-- If xmobar is in your $PATH, and its config is in ~/.xmobarrc you don't
-- need the xmobar path or config file, use: xmproc <- spawnPipe "xmobar"
 
main = do
  xmproc <- spawnPipe "/usr/bin/xmobar /home/sbditto85/.xmobarrc"
  xmonad $ defaultConfig
             { manageHook = manageDocks <+> manageHook defaultConfig
             , layoutHook = avoidStruts  $  layoutHook defaultConfig
             , logHook = dynamicLogWithPP xmobarPP
                         { ppOutput = hPutStrLn xmproc
                        , ppTitle = xmobarColor "green" "" . shorten 50
                         }
             , modMask = mod4Mask     -- Rebind Mod to the Windows key
             } `additionalKeys`
             [ ((mod4Mask .|. shiftMask, xK_z), spawn "xscreensaver-command -lock")
             , ((controlMask, xK_Print), spawn "sleep 0.2; scrot -s")
             , ((0, xK_Print), spawn "scrot")
             ]
